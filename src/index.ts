import * as amqplib from 'amqplib';
import {koinos} from '@koinos/proto-js';
import {Buffer} from "buffer";
import {config} from "./config";
import {ConsumeMessage} from "amqplib";

let connection: any = null;

(async () => {

    console.log(`Attempting to connect to ${config.AMPQ_URL}...`);

    connection = await amqplib.connect(config.AMPQ_URL);

    console.log('Connected');

    const channel = await connection.createChannel();
    console.log('Channel created');

    // Make sure the queue exists
    await channel.assertQueue(config.AMPQ_QUEUE);
    console.log(`Queue ${config.AMPQ_QUEUE} asserted`);

    // Optional step, mostly for testing purposes. For production use you probably don't want to purge the queue
    await channel.purgeQueue(config.AMPQ_QUEUE);
    console.log('Queue purged');

    // Bind our queue to the exchange and routing key to receive proper messages. In this case we will only get block.accepted messages
    await channel.bindQueue(config.AMPQ_QUEUE, config.AMPQ_EXCHANGE, config.AMPQ_ROUTING_KEY);

    await channel.consume(config.AMPQ_QUEUE, (msg: ConsumeMessage) => {
        if (msg !== null) {

            const {block} = koinos.broadcast.block_accepted.decode(msg.content);

            const id = `0x${Buffer.from(block?.id!).toString('hex')}`;

            console.log(`Received block height ${block?.header?.height} with id ${id} containing ${block?.transactions?.length} transactions`);

            // Let's acknowledge the message to indicate that it has been processed properly by the consumer and can be removed from the queue
            channel.ack(msg);
        } else {
            console.log('Consumer cancelled by server');
        }
    });

})();

// Make sure we close the connection when the process is terminated (ctrl+c)
process.on('SIGINT', () => {
    console.log('Closing connection');
    if (connection) {
        connection.close();
    }
});
