export const config = {
    AMPQ_URL: process.env.AMPQ_URL || 'amqp://localhost',
    AMPQ_QUEUE: process.env.AMPQ_QUEUE || 'koinos-boilerplate',
    AMPQ_EXCHANGE: process.env.AMPQ_EXCHANGE || 'koinos.event',
    AMPQ_ROUTING_KEY: process.env.AMPQ_ROUTING_KEY || 'koinos.block.accept'
}
