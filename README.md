# Koinos Microservice Boilerplate

This is a boilerplate for creating a Koinos microservice. It is intended to be used as a part of koinos cluster.

The idea is to connect to AQMP and listen for specific messages. When a message is received, the microservice will process it and display some information about it. In this example, the microservice will display the block height, ID and the number of transactions in the block. You may want to parse transactions or events.

It might be useful for some use cases, like enabling live updates of the blockchain state for the end user via a websocket connection. Imagine a situation in which the user plays a game with someone else and the frontend application may want to react for some events ASAP (like the opponent moves).

This approach is not recommended to calculate the state of the app. In such case, the microservice should be redesigned to sync blocks to not miss anything and handle forks.

# How to use

## Local development

You need to have local koinos node running with the AMQP microservice port binded to your host machine (it is by default). In such case, all you need to do is to install dependencies and run the app locally.

```sh
npm install
```

```sh
npm run start
```

## Docker

You may also want to run this microservice in docker. In such case, you need to build the image first.

```sh
docker build -t koinos-microservice-broadcast-boilerplate:latest .
```

Then you can modify the koinos docker-compose.yml file to use your image as a custom microservice in a cluster:

```yml
koinos_boilerplate:
  image: koinos-microservice-broadcast-boilerplate:latest
  restart: always
  depends_on:
     - amqp
  environment:
     - AMPQ_URL=amqp://amqp:5672
```

Then you can run the cluster with your custom microservice.

```sh
docker compose up
```

# Configuration

The microservice can be configured via environment variables. The following variables are available:

- `AMQP_URL` - the url of the AMQP server (default: `amqp://localhost:5672`). It is recommended to use the name of the service instead of the IP address in case of running it in koinos cluster.
- `AMQP_QUEUE` - the name of the queue to listen for messages (default: `koinos-boilerplate`)
- `AMQP_EXCHANGE` - the name of the exchange to listen for messages (default: `koinos.event`)
- `AMQP_ROUTING_KEY` - the routing key to listen for messages (default: `koinos.block.accept`)
